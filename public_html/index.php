<?php
$app = require dirname(dirname(__FILE__)) . "/web.php";
$accept = filter_input(INPUT_SERVER, "HTTP_ACCEPT");

if ($accept == "application/json") {

    ob_start();
    require dirname(__FILE__) . "/_chat_users_online.php";
    $usersHtml = ob_get_clean();

    ob_start();
    require dirname(__FILE__) . "/_chat_messages.php";
    $messagesHtml = ob_get_clean();

    echo json_encode([
        "usersHtml" => $usersHtml,
        "messagesHtml" => $messagesHtml
    ]);

    die;
}

$uri = filter_input(INPUT_SERVER, "REQUEST_URI");
$uri_href = htmlspecialchars($uri, ENT_QUOTES);
?>
<!doctype html>
<html>
    <head>
        <title>Simple Chat <?php echo $app->version() ?> (author: <?php echo $app->author() ?>)</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="stylesheet.css" />
    </head>
    <body>
        <div class="container">

            <?php /* LEFT PANLE */ ?>
            <div class="widget-users">
                <div id="users">
                    <?php require dirname(__FILE__) . "/_chat_users_online.php"; ?>
                </div><!-- users -->
            </div>

            <?php /* CHAT */ ?>
            <h4>Simple Chat (<a href="<?php echo $uri_href ?>">refresh</a>)</h4>
            <div id="messages" class="widget-messages">
                <?php require dirname(__FILE__) . "/_chat_messages.php"; ?>
            </div><!-- messages -->

            <?php /* SENDING FORM */ ?>
            <div class="widget-form">
                <?php require dirname(__FILE__) . "/_chat_form_send_message.php"; ?>
            </div>

        </div>

        <script src="scripts.js" type="text/javascript"></script>
    </body>
</html>