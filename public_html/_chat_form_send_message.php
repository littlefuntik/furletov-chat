<form action="" method="post" id="send-message-form">
    <input type="text" name="message" id="message" placeholder="Enter message here..." title="Minimum: 2, maximum - 5 characters." autofocus />
    <input type="submit" value="Send Message" />
</form>

<?php
if (! empty($app->recipient)) {
    echo "<div>Send message to <em>{$app->recipient['username']}</em>. <a href=\"?\">Cancel</a></div>";
}
?>
