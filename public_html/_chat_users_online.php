<h4>Online</h4>
<?php
$online_users = $app->getOnlineUsers();

if (empty($online_users)) {
    echo "Users not found.";
}

foreach ($online_users as $user) {
    $pk = $user["id"];
    $username = $user["username"];
    $url = "?recipient=" . urlencode($user["id"]);

    // Check for me.
    if ($app->user->username === $username) {
        echo "<div id=\"user-$pk\" class=\"user\"><small><strong>$username</strong></small></div>";
    } else {
        echo "<div id=\"user-$pk\" class=\"user\"><a href=\"$url\" onclick=\"selectUser(); return false;\"><small>$username</small></a></div>";
    }

}
