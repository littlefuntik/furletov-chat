(function(window) {

    function addEvent(elem, type, handler) {
        if (elem.addEventListener) {
            elem.addEventListener(type, handler, false);
        } else {
            elem.attachEvent("on" + type, handler);
        }
    }

    function getXmlHttp() {
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }

    function sendMessage(event) {
        if (messageField instanceof Element && messageField.nodeName === "INPUT") {
            // Get message text from field
            var msg = encodeURIComponent(messageField.value);

            // Send request with new message data.
            xhr = getXmlHttp();
            xhr.open("POST", window.location.href.toString(), true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.setRequestHeader("X-REQUESTED-WITH", "XMLHttpRequest");
            xhr.setRequestHeader("Accept", "application/json");
            xhr.onreadystatechange = function() {
                if(xhr.readyState == 4 && xhr.status == 200) {

                    try {
                        resp = JSON.parse(xhr.responseText);
                    } catch(e) {
                        alert(e);
                    } finally {
                        messagesContainer.innerHTML = resp.messagesHtml;
                        usersContainer.innerHTML = resp.usersHtml;
                        messageField.value = "";
                    }

                }
            }
            xhr.send("message=" + msg);
        }

        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }

        return false;
    }

    var messageField, form, messagesContainer, usersContainer;

    window.onload = function() {
        // Get elements.
        messageField = document.getElementById("message");
        form = document.getElementById("send-message-form");
        messagesContainer = document.getElementById("messages");
        usersContainer = document.getElementById("users");

        // Send message (AJAX).
        addEvent(form, "submit", sendMessage);
    };

})(window);