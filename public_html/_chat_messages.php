<?php
$posts = $app->getPosts();
$users = $app->getUsers();

if (empty($posts)) {
    echo "Messages not found.";
}

$i = 0;
foreach ($posts as $post) {
    $pk = $post["id"];
    $dt = substr($post["_created"], 11);
    $msg = $post["body"];
    $msg = htmlspecialchars($msg);
    if ($i <= $app->config["chat"]["old_messages"]["count"]) {
        $old_limit = $app->config["chat"]["old_messages"]["message_maxlength"];
        if ($old_limit < strlen($post["body"])) {
            $msg = substr($post["body"], 0, $old_limit) . "...";
        }
    }
    $sender = $post["username"];

    // Check for my post.
    if ($app->user->primaryKey === $post["user_id"]) {
        $sender = "<strong>" . $post["username"] . "</strong>";
    } else {
        $sender_url = "?recipient=" . urlencode($post["user_id"]);
        $sender = "<a href=\"$sender_url\">" . $post["username"] . "</a>";
    }

    // Recipient.
    if (! empty($post["recipient"])) {
        $recipient_url = "?recipient=" . urlencode($post["recipient"]);
        if ($app->user->primaryKey === $post['recipient']) {
            $sender .= " to {$post['recipient_username']}";
        } else {
            $sender .= " to <a href=\"$recipient_url\">" . $post["recipient_username"] . "</a>";
        }
    }

    $usernames = [];
    foreach ($users as &$user) {
        $usernames[$user["username"]] = $user;
    }
    unset($user);

    // Check message for nicknames
    $count = preg_match_all('/(@([\w\d_-]+))\s?/', $msg, $matches);
    if ($count) {
        foreach ($matches[2] as &$v) {
            if (array_key_exists($v, $usernames)) {
                $user = &$usernames[$v];
                $link = "?recipient=" . $user["id"];
                if ($app->user->primaryKey === $user["id"]) {
                    $v = "<strong>@" . $user["username"] . "</strong>";
                } else {
                    $v = "<strong>@<a href=\"$link\">" . $user["username"] . "</a></strong>";
                }
                unset($user);
            }
        }
        $msg = str_replace($matches[1], $matches[2], $msg);
    }

    if (intval($post["ajax"]) === 1) {
        $msg = "<font color=\"green\">$msg</font>";
    }

    echo "<div id=\"post-$pk\"><small>[$dt] $sender:</small> $msg</div>";
    $i++;
}
