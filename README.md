### О проекте ###
Простенький чат, написанный мною. Первый релиз я сделал за пару дней.

### ШАГ 1. Скачать чат ###
```
#!bash

$ cd /path/to/project/
$ git clone https://littlefuntik@bitbucket.org/littlefuntik/furletov-chat.git
```

### ШАГ 2. Импорт базы данных ###

```
#!bash

$ mysql -u'USER' -p'PASSWORD' DATABASE < simpleChat.sql
```

### ШАГ 3. Настраиваем чат ###
```
#!bash

$ vi web-config.php
```

### ШАГ 4. Проверяем работу чата ###

Стучимся из браузера к файлу: /path/to/project/public_html/index.php

### Замечания ###
- Версия PHP должна быть не менее 5.5