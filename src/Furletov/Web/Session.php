<?php
namespace Furletov\Web;

use \PDO as PDO;
use \Furletov\Configuration as Config;

/**
 * Session controller for WEB clients.
 */
class Session extends Config
{
    // One hour for cookie life as default value.
    const DEFAULT_LIFE_TIME = 3600;

    private $_salt = "fo923gd12";
    private $_id = false;

    // Database record data.
    public $row = [];

    /**
     * Class constructor.
     */
    public function __construct($config)
    {
        // Set configuration
        parent::__construct($config);

        if ($this->exists()) {
            $this->extend();
        } else {
            $this->create();
        }
    }

    /**
     * Check for session exists.
     */
    public function exists()
    {
        return !! $this->getID();
    }

    /**
     * Start session.
     */
    public function create()
    {
        // Session exists.
        if ($this->exists()) {
            return false;
        }

        $this->_id = self::generateID();

        // Set cookie for session
        setcookie($this->cookieKey(), $this->_id, time() + $this->lifetime());
    }

    /**
     * Stop session.
     */
    public function destroy()
    {
        // Session not exists.
        if (! $this->exists()) {
            return false;
        }

        // Remove cookie for session (expired).
        setcookie($this->cookieKey(), "", time() - 3600);
    }

    /**
     * Extend the lifetime of the session.
     */
    public function extend()
    {
        // Session not exists.
        if (! $this->exists()) {
            return false;
        }

        // Session ID.
        $id = $this->getID();

        setcookie($this->cookieKey(), $id, time() + $this->lifetime());
    }

    /**
     * Get cookie key.
     */
    public function cookieKey()
    {
        $prefix = array_key_exists("cookie_prefix", $this->config)
                    ? $this->config["cookie_prefix"]
                    : "";
        return $prefix . $this->_salt;
    }

    /**
     * Get cookie life time.
     * @return integer Session life time (miliseconds).
     */
    public function lifetime()
    {
        $time = array_key_exists("lifetime", $this->config)
                    ? $this->config["lifetime"]
                    : self::DEFAULT_LIFE_TIME;
        return $time;
    }

    /**
     * Get session ID.
     */
    public function getID()
    {
        $cookie_value = filter_input(INPUT_COOKIE, $this->cookieKey());
        if (!$cookie_value && !$this->_id) {
            return false;
        } elseif ($this->_id) {
            return $this->_id;
        } elseif ($cookie_value) {
            return $cookie_value;
        }
    }

    /**
     * Generate new unique ID.
     */
    public static function generateID()
    {
        return md5(uniqid(microtime()));
    }

    /**
     * Get session record from database.
     */
    public function dbGet(&$dbConnection)
    {
        // Session not exists.
        if (! $this->exists()) {
            return [];
        }

        // Session ID.
        $id = $this->getID();

        // PDO instance object.
        $dbh = $dbConnection->getConnection();

        // Get session record from database.
        $sth = $dbh->prepare("SELECT * FROM `session` WHERE `sessid` = ?");
        $sth->execute([$id]);

        if ($sth->rowCount() > 0) {
            $row = $sth->fetch(PDO::FETCH_ASSOC);
            return $row;
        }

        return [];
    }

    /**
     * Check for session expired.
     */
    public function dbExpired(&$dbConnection)
    {
        // Session not exists.
        if (! $this->exists()) {
            return true;
        }

        // Session ID.
        $id = $this->getID();

        // PDO instance object.
        $dbh = $dbConnection->getConnection();

        // Get expired session record from database.
        $sth = $dbh->prepare("SELECT COUNT(*) as `count` FROM `session` WHERE `sessid` = ? AND DATE_ADD(`_updated`, INTERVAL ? SECOND) <= NOW()");
        $sth->execute([$id, $this->lifetime()]);
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        if ($row["count"] > 0) {
            var_dump($sth->rowCount());
            return true;
        }

        $row = $sth->fetch(PDO::FETCH_ASSOC);

    }

    /**
     * Synchronize current sesson with database.
     */
    public function dbSync(&$dbConnection)
    {
        // Session not exists.
        if (! $this->exists()) {
            return false;
        }

        $remove = false;

        if ($this->dbExpired($dbConnection)) {
            var_dump('Expired');
            $remove = true;
        }

        // Session ID.
        $id = $this->getID();

        // PDO instance object.
        $dbh = $dbConnection->getConnection();

        // Get session record from database.
        $sth = $dbh->prepare("SELECT * FROM `session` WHERE `sessid` = ?");
        $sth->execute([$id]);
        $count = $sth->rowCount();

        if (!$remove && $count > 1) {
            // Remove all records from database where count for current sessid greated 1.
            $remove = true;
        }

        if ($remove) {
            // Remove current user.
            // $sth = $dbh->prepare("
            //     DELETE FROM `user` WHERE `id` IN (
            //         SELECT `user_id` FROM `session` WHERE `sessid` = ?
            //     )
            // ");
            // $sth->execute([$id]);

            // Remove current session ID.
            $sth = $dbh->prepare("
                DELETE FROM `session` WHERE `sessid` = ?
            ");
            $sth->execute([$id]);

            $count = 0;

            $this->destroy();
            $this->create();
            $id = $this->getID();
        }

        $row = $count ? $sth->fetch(PDO::FETCH_ASSOC) : [];

        // Additional update query string from row parameters
        $additionalUpdateSql = "";
        foreach ($this->row as $k => $v) {
            // Clear bad symbols.
            $k = preg_replace("/[^\w\d-_]+/i", "", $k);
            // If column exists and value changed...
            if (! empty($k) && array_key_exists($k, $row) && $row[$k] != $v) {
                $additionalUpdateSql .= ", `$k` = :$k";
                $input_parameters[":$k"] = $v;
            }
        }

        // ... and set session ID.
        $input_parameters[":sessid"] = $id;

        if ($count) {
            // Update session in database.
            $sth = $dbh->prepare("UPDATE `session` SET `_updated` = NOW()" . $additionalUpdateSql . " WHERE `sessid` = :sessid");
            $sth->execute($input_parameters);
        } else {
            // Create session in database.
            $sth = $dbh->prepare("INSERT INTO `session` SET `sessid` = :sessid, `_updated` = NOW()" . $additionalUpdateSql);
            $sth->execute($input_parameters);
        }

        $this->row = $this->dbGet($dbConnection);

        return true;
    }

    /**
     * Remove all expired sessions
     */
    public function dbExpiredRemoveAll(&$dbConnection)
    {
        // PDO instance object.
        $dbh = $dbConnection->getConnection();

        try {
            $dbh->beginTransaction();

            // Remove all users from expired sessions.
            // $sth = $dbh->prepare("
            //     DELETE FROM `user` WHERE `id` IN (
            //         SELECT `user_id` FROM `session` WHERE DATE_ADD(`_updated`, INTERVAL ? SECOND) <= NOW()
            //     )
            // ");
            // $sth->execute([$this->lifetime()]);

            // Remove all expired sessions.
            $sth = $dbh->prepare("
                DELETE FROM `session` WHERE DATE_ADD(`_updated`, INTERVAL ? SECOND) <= NOW()
            ");
            $sth->execute([$this->lifetime()]);

            $dbh->commit();
        } catch(PDOExecption $e) {
            $dbh->rollback();
            throw "Error: " . $e->getMessage();
        }
    }
}