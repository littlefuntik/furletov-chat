<?php
namespace Furletov\Web;

use \PDO as PDO;
use \Furletov\Configuration as Config;

/**
 * User class
 */
class User extends Config
{
    // Errors
    const ERR_USER_EXISTS = "User exists.";

    // Primary key
    public $primaryKey = null;
    public $username = null;

    /**
     * Class constructor.
     */
    public function __construct($config)
    {
        // Set configuration
        parent::__construct($config);
    }

    /**
     * Initialize user
     */
    public function initByPk($pk, &$dbConnection)
    {
        // PDO instance object.
        $dbh = $dbConnection->getConnection();

        $sth = $dbh->prepare("SELECT * FROM `user` WHERE `id` = ?");
        $sth->execute([$pk]);
        if ($sth->rowCount() < 1) {
            return false;
        }
        $row = $sth->fetch(PDO::FETCH_ASSOC);

        $this->primaryKey = $pk;
        $this->username = $row['username'];
        return true;
    }

    /**
     * Create user
     */
    public static function create($username, &$dbConnection)
    {
        if (empty($username)) {
            return false;
        }

        // PDO instance object.
        $dbh = $dbConnection->getConnection();

        $sth = $dbh->prepare("SELECT COUNT(*) AS `count` FROM `user` WHERE `username` = ?");
        $sth->execute([$username]);
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        if ($row["count"] > 0) {
            throw ERR_USER_EXISTS;
        }

        $sth = $dbh->prepare("INSERT INTO `user` SET `username` = ?");
        try {
            $dbh->beginTransaction();
            $sth->execute([$username]);
            $pk = $dbh->lastInsertId();
            $dbh->commit();
        } catch(PDOExecption $e) {
            $dbh->rollback();
            throw "Error: " . $e->getMessage();
        }

        return $pk;
    }

    /**
     * Usernames list
     */
    public static function usernames(&$dbConnection)
    {
        // PDO instance object.
        $dbh = $dbConnection->getConnection();

        // Usernames list
        $ulist = [];

        $sth = $dbh->prepare("SELECT `username` FROM `user`");
        $sth->execute();
        if ($sth->rowCount() > 0) {
            do {
                $row = $sth->fetch(PDO::FETCH_ASSOC);
                if (! empty($row)) {
                    $ulist[] = $row["username"];
                }
            } while ($row);
        }
        return $ulist;
    }

}