<?php
namespace Furletov\Database;

use \PDO as PDO;
use \Furletov\Configuration as Config;

/**
 * Database connector controller.
 */
class PDOConnector extends Config
{
    protected $dbh = false;

    /**
     * Class constructor.
     */
    function __construct($config)
    {
        // Set configuration
        parent::__construct($config);
    }

    /**
     * Connect to database.
     */
    public function connect()
    {
        try {
            $this->dbh = new PDO($this->config["dsn"], $this->config["user"], $this->config["password"]);
        } catch (PDOException $e) {
            throw "Connection error: " . $e->getMessage();
        }
    }

    /**
     * Check for database connection.
     * @return boolean Connected (true) or not connected (false).
     */
    public function connected()
    {
        return !! $this->dbh;
    }

    /**
     * Get PDO instance for connection.
     * @return PDO PDO Connection object.
     */
    public function getConnection()
    {
        return $this->dbh;
    }
}