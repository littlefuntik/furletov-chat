<?php
namespace Furletov;

class Configuration
{
    // Errors
    const ERROR_CONFIG_TYPE = "Config type must be array.";

    public $config = [];

    /**
     * Class constructor.
     */
    public function __construct($config)
    {
        $this->setConfig($config);
    }

    /**
     * Set configurations for instance.
     * Validate configs and throw error if bad it.
     */
    protected function setConfig($config)
    {
        if (gettype($config) !== 'array') {
            throw self::ERROR_CONFIG_TYPE;
        }
        $this->config = $config;
    }
}