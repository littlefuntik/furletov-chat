<?php
namespace Furletov\Chat;

use \Furletov\Configuration as Config;
use \Furletov\Web\Session as Session;
use \Furletov\Web\User as User;
use \Furletov\Database\PDOConnector as DatabaseConnector;
use \PDO as PDO;

/**
 * Class Application for chat project.
 */
class Application extends Config
{
    // Warnings
    const WARNING_AUTHOR = "No author.";
    const WARNING_VERSION = "No version.";

    // Database connector.
    public $dbConnection;

    // Session instance.
    public $session;

    // User instance
    public $user;

    // Other
    public $serverRequestMethod;

    // Recipient
    public $recipient = null;

    /**
     * Class constructor.
     */
    public function __construct($config)
    {
        // Set configuration
        parent::__construct($config);

        // HTTP
        $this->setHeaders();
        $this->serverRequestMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');

        // Logic
        $this->dbConnect();
        $this->initSession();
        $this->initUser($this->session);
        $this->initRecipient();

        // Send message if need.
        if ($this->serverRequestMethod === "POST") {
            if ($this->user->primaryKey) {
                $msg_maxlen = $this->config["chat"]["message_maxlength"];
                $msg_minlen = $this->config["chat"]["message_minlength"];
                $msg = filter_input(INPUT_POST, 'message');
                if ($msg) {
                    if (strlen($msg) >= $msg_minlen && strlen($msg) <= $msg_maxlen) {
                        $dbh = $this->dbConnection->getConnection();

                        try {
                            // Add message to the database.
                            $recipient = !empty($this->recipient) ? $this->recipient["id"] : 0;
                            $dbh->beginTransaction();
                            $sth = $dbh->prepare("INSERT INTO `post` SET `body` = ?, `user_id` = ?, `recipient` = ?, ajax = ?");
                            $sth->execute([
                                $msg,
                                $this->user->primaryKey,
                                $recipient,
                                intval($this->isAjaxRequest())
                            ]);
                            $dbh->commit();
                        } catch(PDOExecption $e) {
                            $dbh->rollback();
                            throw "Error: " . $e->getMessage();
                        }
                    }
                }
            }
        }
    }

    /**
     * Set headers.
     */
    function setHeaders()
    {
        // Charset
        header("Content-Type: text/html; charset=utf-8");

        // Content Security Policy
        $policies = "default-src 'self'; script-src 'self'; style-src 'self'; connect-src 'self'; media-src 'self'; object-src 'self'; frame-src 'self';";
        if (! empty($policies)) {
            header("Content-Security-Policy: " . $policies);    // W3C
            header("X-Content-Security-Policy: " . $policies);  // Firefox 4-23, IE 10-11
            header("X-Webkit-CSP: " . $policies);               // Chrome 14-25, Safari 5.1-7
        }

        // Disable HTTP Cache
        header("Content-Type: text/html");
        header("Expires: 0");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    /**
     * Check for Ajax request
     */
    public function isAjaxRequest()
    {
        $xhr = filter_input(INPUT_SERVER, "HTTP_X_REQUESTED_WITH");
        return ( !empty($xhr) && strtolower($xhr) == 'xmlhttprequest' );
    }

    /**
     * Get author name.
     *
     * @return string Author name.
     */
    public function author()
    {
        if (! array_key_exists("author", $this->config)) {
            throw self::WARNING_AUTHOR;
        }
        return $this->config["author"];
    }

    /**
     * Get application version.
     *
     * @return string Version.
     */
    public function version()
    {
        if (! array_key_exists("version", $this->config)) {
            throw self::WARNING_VERSION;
        }
        return $this->config["version"];
    }

    /**
     * Connection to database.
     */
    protected function dbConnect()
    {
        $connection = new DatabaseConnector($this->config["database"]);
        $connection->connect();
        $this->dbConnection = &$connection;
    }

    /**
     * Create or restore session.
     */
    protected function initSession()
    {
        $session = new Session($this->config["session"]);
        $session->dbSync($this->dbConnection);
        $this->session = &$session;

        $session->dbExpiredRemoveAll($this->dbConnection);
    }

    /**
     * Initialize user
     */
    protected function initUser(&$session)
    {
        $user = new User($this->config["user"]);

        $pk = array_key_exists("user_id", $session->row) ? $session->row["user_id"] : "";
        if (!$pk || !$user->initByPk($pk, $this->dbConnection)) {
            // Create new user.
            $pk = $this->registerUser();
            $user->initByPk($pk, $this->dbConnection);
            // Save user primary key in session.
            $session->row['user_id'] = $pk;
            $session->dbSync($this->dbConnection);
        }

        $this->user = &$user;
        return true;
    }

    /**
     * Register new user
     */
    protected function registerUser()
    {
        // Get all usernames.
        $ulist = User::usernames($this->dbConnection);

        // Get random username.
        $rnd = array_rand($this->config["user"]["list_logins"], 1);
        $login = $this->config["user"]["list_logins"][$rnd];

        // Add suffix(number) to username if not unique.
        $tmp_login = $login;
        $i = 0;
        while (in_array($tmp_login, $ulist)) {
            $i++;
            $tmp_login = $login . $i;
        }

        $login = $tmp_login;

        $pk = User::create($login, $this->dbConnection);
        return $pk;
    }

    /**
     * Get message recipient from request.
     */
    public function initRecipient()
    {
        $recipient = filter_input(INPUT_GET, 'recipient');

        // Get recipient from database.
        if (! empty($recipient) && intval($recipient) !== intval($this->user->primaryKey)) {
            $dbh = $this->dbConnection->getConnection();

            $sth = $dbh->prepare("SELECT * FROM `user` WHERE `id` = :id");
            $sth->bindValue(':id', $recipient, PDO::PARAM_INT);
            $sth->execute();
            if ($sth->rowCount() > 0) {
                $this->recipient = $sth->fetch(PDO::FETCH_ASSOC);
            }
        }

        return $this->recipient;
    }

    /**
     * Get Posts
     */
    public function getPosts()
    {
        // PDO instance object.
        $dbh = $this->dbConnection->getConnection();

        $sth = $dbh->prepare("
            SELECT
                `p`.*,
                `u`.`username` as `username`,
                `u1`.`username` as `recipient_username`
            FROM
                `post` as `p`
            LEFT JOIN
                `user` as `u`
                ON
                    `u`.`id` = `p`.`user_id`
            LEFT JOIN
                `user` as `u1`
                ON
                    `u1`.`id` = `p`.`recipient`
            ORDER BY
                `p`.`_created` DESC
            LIMIT :limit
        ");
        $sth->bindValue(':limit', $this->config["chat"]["display_messages_count"], PDO::PARAM_INT);
        $sth->execute();
        if ($sth->rowCount() > 0) {
            $posts = $sth->fetchAll(PDO::FETCH_ASSOC);
            $posts = array_reverse($posts);
            return $posts;
        } else {
            return [];
        }
    }

    /**
     * Get online users list
     */
    public function getOnlineUsers()
    {
        // PDO instance object.
        $dbh = $this->dbConnection->getConnection();

        $sth = $dbh->prepare("
            SELECT
                `s`.*,
                `u`.`username` as `username`,
                `u`.`id` as `id`
            FROM
                `session` as `s`
            LEFT JOIN
                `user` as `u`
                ON
                    `u`.`id` = `s`.`user_id`
            WHERE
                DATE_ADD(`s`.`_updated`, INTERVAL ? SECOND) > NOW()
            ORDER BY
                `s`.`_created` ASC
        ");
        $sth->execute([$this->session->lifetime()]);
        if ($sth->rowCount() > 0) {
            $users = $sth->fetchAll(PDO::FETCH_ASSOC);
            return $users;
        } else {
            return [];
        }
    }

    /**
     * Get all users list
     */
    public function getUsers()
    {
        $users = [];

        // PDO instance object.
        $dbh = $this->dbConnection->getConnection();

        $sth = $dbh->prepare("SELECT * FROM `user`");
        $sth->execute();
        if ($sth->rowCount() > 0) {
            $users = $sth->fetchAll(PDO::FETCH_ASSOC);
        }

        return $users;
    }
}
