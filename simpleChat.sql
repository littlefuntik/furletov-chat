-- phpMyAdmin SQL Dump
-- version 4.3.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 21 2015 г., 13:02
-- Версия сервера: 10.0.15-MariaDB-log
-- Версия PHP: 5.6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `simpleChat`
--

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(10) unsigned NOT NULL COMMENT 'Уникальный идентификатор сообщения',
  `body` text NOT NULL COMMENT 'Текст сообщения',
  `user_id` int(11) NOT NULL COMMENT 'Владелец сообщения',
  `recipient` int(11) NOT NULL COMMENT 'Получатель',
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время отправки сообщения',
  `ajax` int(1) NOT NULL DEFAULT '0' COMMENT 'Сообщение отправлено средствами Ajax'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='Сообщения';

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `sessid` text NOT NULL COMMENT 'Ключ сессии',
  `user_id` int(11) NOT NULL,
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания',
  `_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Время последней активности'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сессии пользователей';

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL COMMENT 'Уникальный идентификатор пользователя',
  `username` varchar(32) NOT NULL COMMENT 'Логин пользователя'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `session`
--
ALTER TABLE `session`
  ADD KEY `upd_index` (`_updated`), ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username_index` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор сообщения',AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор пользователя',AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
