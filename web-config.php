<?php /** @return Chat application configurations */
return [

    "version" => "1.0",
    "author" => "Hryhorii Furletov",

    "database" => [
        "dsn" => "mysql:dbname=simpleChat;host=localhost",
        "user" => "root",
        "password" => "111",
    ],

    "session" => [
        "cookie_prefix" => "oi4fg439",
        "lifetime" => 3600 * 8 // seconds
    ],

    "user" => [
        "list_logins" => [
            "Chappy", "Grizli", "Cat", "Sarafan4ik",
            "Python", "Perl", "Mongo", "Lime", "Teremok"
        ]
    ],

    "chat" => [
        "message_minlength" => 1,
        "message_maxlength" => 50,
        "display_messages_count" => 50,
        "old_messages" => [
            "count" => 10,
            "message_maxlength" => 5
        ]
    ]
];