<?php /** @return Chat application */

require dirname(__FILE__) . "/vendor/autoload.php";
return new \Furletov\Chat\Application(
	require dirname(__FILE__) . "/web-config.php");